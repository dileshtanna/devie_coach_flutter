import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'reusable_button.dart';

class IntroPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Hero(
                      tag: 'logo',
                      child: Container(
                        margin: EdgeInsets.only(top: 40.0),
                        child: CircleAvatar(
                          radius: 100.0,
                          backgroundImage: AssetImage('images/logo.png'),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10.0),
                      child: Text(
                        'Devie',
                        style: TextStyle(
                          color: Color.fromRGBO(91, 131, 195, 1),
                          fontFamily: 'Montserrat-Regular',
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: Text(
                        'Have we met before?',
                        style: TextStyle(
                          fontFamily: 'Montserrat-Regular',
                          fontWeight: FontWeight.bold,
                          fontSize: 26.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(20.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: ReusableButton(
                        text: 'Yes 😁',
                        onPressed: () {
                          print('Yes');
                        },
                      ),
                    ),
                    SizedBox(
                      width: 20.0,
                    ),
                    Expanded(
                      child: ReusableButton(
                        text: 'I\'m new here!',
                        onPressed: () {
                          Navigator.pushNamed(context, '/seven');
                          print('No');
                        },
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
