import 'package:flutter/material.dart';
import 'tour_page_one.dart';
import 'tour_page_two.dart';
import 'tour_page_three.dart';
import 'tour_page_four.dart';
import 'tour_page_five.dart';
import 'intro_page.dart';
import 'sign_up_one.dart';
import 'sign_up_two.dart';
import 'sign_up_three.dart';
import 'sign_up_four.dart';
import 'sign_up_six.dart';
import 'sign_up_five.dart';

void main() {
  runApp(MaterialApp(
      initialRoute: '/',
      theme: ThemeData(
        pageTransitionsTheme: PageTransitionsTheme(builders: {
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
        }),
      ),
      routes: {
        '/': (context) => DevieApp(),
        '/second': (context) => SecondRoute(),
        '/third': (context) => ThirdRoute(),
        '/four': (context) => FourthRoute(),
        '/five': (context) => FifthRoute(),
        '/six': (context) => IntroPage(),
        '/seven': (context) => SignUpOne(),
        '/eight': (context) => SignUpTwo(),
        '/nine': (context) => SignUpThree(),
        '/ten': (context) => SignUpFour(),
        '/eleven': (context) => SignUpFive(),
        '/twelve': (context) => SignUpSix(),
      }));
}

//testing git
