import 'package:flutter/material.dart';

class ReusableButton extends StatelessWidget {
  ReusableButton({@required this.text, @required this.onPressed}) {}

  final String text;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(10.0),
        child: Text(text),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: Color.fromRGBO(91, 131, 195, 1),
              width: 2.0,
            ),
            borderRadius: BorderRadius.circular(16.0)),
      ),
    );
  }
}
