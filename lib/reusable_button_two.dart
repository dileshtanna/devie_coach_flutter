import 'package:flutter/material.dart';

class ReusableButtonTwo extends StatelessWidget {
  ReusableButtonTwo(this.navRoute, this.text) {}
  final String navRoute, text;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 30.0),
      child: FlatButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
          child: Text(text,
              style: TextStyle(
                fontFamily: 'Montserrat-Regular',
                fontSize: 18,
                color: Colors.white,
              )),
          color: Color.fromRGBO(91, 131, 195, 1),
          onPressed: () {
            print(navRoute);
            Navigator.pushNamed(context, navRoute);
          }),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey[600],
            offset: Offset(4.0, 4.0),
            blurRadius: 15.0,
            spreadRadius: 1.0,
          ),
        ],
      ),
    );
  }
}
