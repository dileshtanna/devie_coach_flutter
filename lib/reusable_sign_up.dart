//import 'package:flutter/material.dart';
//import 'reusable_button_two.dart';
//
//class ReusableSignUp extends StatelessWidget {
//  ReusableSignUp(this.text, this.navRoute) {}
//  final String text, navRoute;
//  @override
//  Widget build(BuildContext context) {
//    return SafeArea(
//      child: Scaffold(
//        body: Column(
//          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//          children: <Widget>[
//            Center(
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.center,
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                children: <Widget>[
//                  Container(
//                    child: Column(
//                      children: <Widget>[
//                        Container(
//                          padding: EdgeInsets.only(top: 40.0),
//                          child: CircleAvatar(
//                            backgroundImage: AssetImage('images/logo.png'),
//                            radius: 100.0,
//                          ),
//                        ),
//                        SizedBox(
//                          height: 9.0,
//                        ),
//                        Container(
//                          child: Text(
//                            'Devie',
//                            style: TextStyle(
//                              color: Color.fromRGBO(91, 131, 195, 1),
//                              fontFamily: 'Montserrat-Regular',
//                              fontSize: 30.0,
//                              fontWeight: FontWeight.bold,
//                            ),
//                          ),
//                        ),
//                        SizedBox(
//                          height: 55.0,
//                        ),
//                        Container(
//                          child: Text(
//                            text,
//                            style: TextStyle(
//                              fontFamily: 'Montserrat-Regular',
//                              fontSize: 16.0,
//                              fontWeight: FontWeight.w600,
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  )
//                ],
//              ),
//            ),
//            ReusableButtonTwo(navRoute, 'Next'),
//          ],
//        ),
//      ),
//    );
//  }
//}
//

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'reusable_button_two.dart';

class ReusableSignUp extends StatelessWidget {
  ReusableSignUp(
      {@required this.text, @required this.navRoute, @required this.child}) {}
  final String text, navRoute;
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: <Widget>[
                Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Column(
                          children: <Widget>[
                            Hero(
                              tag: 'logo',
                              child: Container(
                                padding: EdgeInsets.only(top: 40.0),
                                child: CircleAvatar(
                                  backgroundImage:
                                      AssetImage('images/logo.png'),
                                  radius: 100.0,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 9.0,
                            ),
                            Container(
                              child: Text(
                                'Devie',
                                style: TextStyle(
                                  color: Color.fromRGBO(91, 131, 195, 1),
                                  fontFamily: 'Montserrat-Regular',
                                  fontSize: 30.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  left: 40.0, right: 40.0, top: 55.0),
                              child: Text(
                                text,
                                style: TextStyle(
                                  fontFamily: 'Montserrat-Regular',
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(left: 40.0, top: 20.0),
                    child: child),
              ],
            ),
            Container(
              child: ReusableButtonTwo(navRoute, 'Next'),
            ),
          ],
        ),
      ),
    );
  }
}
