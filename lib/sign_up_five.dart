import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'reusable_sign_up.dart';

class SignUpFive extends StatefulWidget {
  SignUpFive({@required this.text, @required this.onPressed}) {}

  final String text;
  final Function onPressed;
  @override
  _SignUpFiveState createState() => _SignUpFiveState();
}

class _SignUpFiveState extends State<SignUpFive> {
  String textToDisplay = 'Time';
  TimeOfDay time = new TimeOfDay.now();
  Future<Null> selectTime(BuildContext context) async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: time);

    if (picked != null && picked == time) {
      print('Time selected: ${time.toString()}');
      setState(() {
        time = picked;
        textToDisplay = '${time.hour}:${time.minute}';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return ReusableSignUp(
        text: 'When should Devie check-in with you?',
        navRoute: '/twelve',
        child: Column(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                selectTime(context);
              },
              child: Container(
                margin: EdgeInsets.only(right: 40.0),
                alignment: Alignment.center,
                padding: EdgeInsets.all(10.0),
                child: Text(textToDisplay),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Color.fromRGBO(91, 131, 195, 1),
                      width: 2.0,
                    ),
                    borderRadius: BorderRadius.circular(0.0)),
              ),
            ),
            //  Text(time.toString()),
          ],
        ));
  }
}
