import 'package:flutter/material.dart';
import 'reusable_sign_up.dart';

class SignUpFour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ReusableSignUp(
      text: 'Enter the OTP sent to your email address',
      navRoute: '/eleven',
      child: Column(
        children: <Widget>[
          Container(
//            color: Colors.black,
            width: double.infinity,
            margin: EdgeInsets.only(right: 40.0),
            child: TextField(
              style: TextStyle(
                color: Color.fromRGBO(91, 131, 195, 1),
                fontFamily: 'Montserrat-Regular',
                fontSize: 18.0,
                fontWeight: FontWeight.w600,
              ),
              decoration: InputDecoration(
                hintText: 'OTP',
                hintStyle: TextStyle(
                  fontFamily: 'Montserrat-Regular',
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(162, 162, 162, 1),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: Color.fromRGBO(162, 162, 162, 1)),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: Color.fromRGBO(162, 162, 162, 1)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
