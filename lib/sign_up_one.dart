import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'reusable_sign_up.dart';

class SignUpOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ReusableSignUp(
      text: 'What should Devie call you?',
      navRoute: '/eight',
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
//            color: Colors.black,
            width: double.infinity,
            margin: EdgeInsets.only(right: 40.0),
            child: TextField(
              style: TextStyle(
                color: Color.fromRGBO(91, 131, 195, 1),
                fontFamily: 'Montserrat-Regular',
                fontSize: 18.0,
                fontWeight: FontWeight.w600,
              ),
              decoration: InputDecoration(
                hintText: 'Screen Name',
                hintStyle: TextStyle(
                  fontFamily: 'Montserrat-Regular',
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(162, 162, 162, 1),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: Color.fromRGBO(162, 162, 162, 1)),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: Color.fromRGBO(162, 162, 162, 1)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
