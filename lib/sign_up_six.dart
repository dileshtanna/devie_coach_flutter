import 'package:flutter/material.dart';
import 'reusable_sign_up.dart';

class SignUpSix extends StatefulWidget {
  @override
  _SignUpSixState createState() => _SignUpSixState();
}

class _SignUpSixState extends State<SignUpSix> {
  String dropdownValue;
  @override
  Widget build(BuildContext context) {
    return ReusableSignUp(
        text:
            'How many children do you have who are between six months and five years old?',
        navRoute: '/thirteen',
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(
                right: 40.0,
              ),
              child: Container(
                child: DropdownButton<String>(
                  hint: Text(
                    'Count',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16.0,
                      fontFamily: 'Montserrat-Regular',
                      color: Color.fromRGBO(162, 162, 162, 1),
                    ),
                  ),
                  value: dropdownValue,
                  icon: Icon(Icons.arrow_drop_down),
                  iconSize: 24,
                  iconEnabledColor: Color.fromRGBO(162, 162, 162, 1),
                  elevation: 16,
                  style: TextStyle(
                    color: Color.fromRGBO(162, 162, 162, 1),
                  ),
                  underline: Container(
                    height: 1,
                    color: Color.fromRGBO(162, 162, 162, 1),
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      dropdownValue = newValue;
                    });
                  },
                  items: <String>['0', '1', '2', '3']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ),
            ),
          ],
        ));
  }
}
