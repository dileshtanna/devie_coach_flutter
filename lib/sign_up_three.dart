import 'package:flutter/material.dart';
import 'reusable_sign_up.dart';
import 'package:flutter/widgets.dart';

class SignUpThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ReusableSignUp(
        text: 'What is your Email ID?',
        navRoute: '/ten',
        child: Column(
          children: <Widget>[
            Container(
//            color: Colors.black,
              width: double.infinity,
              margin: EdgeInsets.only(right: 40.0),
              child: TextField(
                style: TextStyle(
                  color: Color.fromRGBO(91, 131, 195, 1),
                  fontFamily: 'Montserrat-Regular',
                  fontSize: 18.0,
                  fontWeight: FontWeight.w600,
                ),
                decoration: InputDecoration(
                  hintText: 'Email ID',
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 16.0,
                    fontFamily: 'Montserrat-Regular',
                    color: Color.fromRGBO(162, 162, 162, 1),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromRGBO(162, 162, 162, 1)),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromRGBO(162, 162, 162, 1)),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 52.0,
            ),
            Container(
              margin: EdgeInsets.only(right: 40.0),
              child: Text(
                'We have sent a One-Time-Password (OTP) to your email address.',
                style: TextStyle(
                  fontFamily: 'Montserrat-Regular',
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                  color: Color(0xFF53953B),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
