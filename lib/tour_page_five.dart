import 'package:flutter/material.dart';
import 'tour_page_skeleton.dart';

class FifthRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: TourPageSkeleton(
          'images/TourFive.png',
          'Private, Evidence-based',
          'Your data is private and all content comes from Peeple, a non-profit organization whose parenting programs are proven to improve confidence and children\'s developmental outcomes.',
          '/six',
          5,
        ),
      ),
    );
  }
}
