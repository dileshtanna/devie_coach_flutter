import 'package:flutter/material.dart';
import 'tour_page_skeleton.dart';

class FourthRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: TourPageSkeleton(
          'images/TourFour.png',
          'Develop skills and habits',
          'Find the parenting techniques that work for your child by trying activities that fit into your day',
          '/five',
          4,
        ),
      ),
    );
  }
}
