import 'package:flutter/material.dart';
import 'tour_page_skeleton.dart';

class DevieApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: TourPageSkeleton(
            'images/Hi.png',
            'Hi, I\'m Devie',
            'A personal parenting coach who supports you to help your child grow.',
            '/second',
            1),
      ),
    );
  }
}
