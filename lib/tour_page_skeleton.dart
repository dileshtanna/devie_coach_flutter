import 'package:flutter/material.dart';

class ProgressBarCircle extends StatelessWidget {
  ProgressBarCircle(this.color) {}
  final Color color;
  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: color,
      radius: 4.5,
    );
  }
}

class TourPageSkeleton extends StatelessWidget {
  TourPageSkeleton(this.imagePath, this.headerText, this.subText, this.navRoute,
      this.pageNumber) {}
  final String imagePath, headerText, subText, navRoute;
  final int pageNumber;

  @override
  Widget build(BuildContext context) {
    List<Widget> progressBarCircles = [
      ProgressBarCircle(Colors.grey),
      ProgressBarCircle(Colors.grey),
      ProgressBarCircle(Colors.grey),
      ProgressBarCircle(Colors.grey),
      ProgressBarCircle(Colors.grey)
    ];
    progressBarCircles[pageNumber - 1] =
        ProgressBarCircle(Color.fromRGBO(91, 131, 195, 1));
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
//              color: Colors.black,
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 140.0),
                child: Hero(
                  tag: 'logo',
                  child: CircleAvatar(
                    radius: 100.0,
                    backgroundImage: AssetImage(imagePath),
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                margin: EdgeInsets.all(10.0),
                child: Text(
                  headerText,
                  style: TextStyle(
                    fontFamily: 'Montserrat-Regular',
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(horizontal: 40.0),
//                height: 115.0,
                width: double.infinity,
                child: Text(
                  subText,
                  style: TextStyle(
                    fontFamily: 'Montserrat-Regular',
                    fontSize: 18.0,
                    color: Colors.grey,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Container(
                width: 80.0,
                //margin: EdgeInsets.symmetric(vertical:0.0, horizontal: 100.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: progressBarCircles,
                ),
              ),
            ],
          ),
        ),
        Container(
          width: double.infinity,
          margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 30.0),
          child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
              padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
              child: Text('Get Started',
                  style: TextStyle(
                    fontFamily: 'Montserrat-Regular',
                    fontSize: 18,
                    color: Colors.white,
                  )),
              color: Color.fromRGBO(91, 131, 195, 1),
              onPressed: () {
                print(navRoute);
                Navigator.pushNamed(context, navRoute);
              }),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey[600],
                offset: Offset(4.0, 4.0),
                blurRadius: 15.0,
                spreadRadius: 1.0,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
