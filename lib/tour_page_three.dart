import 'package:flutter/material.dart';
import 'tour_page_skeleton.dart';

class ThirdRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: TourPageSkeleton(
          'images/TourThree.png',
          'Get advice',
          'Get personalized recommendations based on your parenting hopes and challenges',
          '/four',
          3,
        ),
      ),
    );
  }
}
