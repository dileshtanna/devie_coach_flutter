import 'package:flutter/material.dart';
import 'tour_page_skeleton.dart';

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: TourPageSkeleton(
          'images/TourTwo.png',
          'Learn about your child\'s development',
          'Understand and enjoy this transformative stage of your child\'s life',
          '/third',
          2,
        ),
      ),
    );
  }
}
